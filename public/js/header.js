var Header = React.createClass({

  render : function() {
    if (location.href.includes("/biz") || location.href.includes("/supplier") || location.href.includes("/shop") || location.href.includes("/post"))
      return React.createElement("div", {className:"header"},
        React.createElement("ul", {},
          React.createElement("img", {src:"../img/logo.png"}),
          React.createElement("li", {}, React.createElement("a", {href:"../biz/"}, "Manage Business")),
          React.createElement("li", {}, React.createElement("a", {href:"../shop/"}, "Shop")),
          React.createElement("li", {}, React.createElement("a", {href:"../supplier/"}, "Fulfill Supply Requests"))
        )
      );
      return React.createElement("div", {className:"header"},
        React.createElement("ul", {},
          React.createElement("img", {src:"./img/logo.png"}),
          React.createElement("li", {}, React.createElement("a", {href:"./biz/"}, "Manage Business")),
          React.createElement("li", {}, React.createElement("a", {href:"./shop/"}, "Shop")),
          React.createElement("li", {}, React.createElement("a", {href:"./supplier/"}, "Fulfill Supply Requests"))
        )
      );
  }

});

window.onload = function() {
  ReactDOM.render(
    React.createElement(Header, {}),
    document.getElementById("header")
  );
}
